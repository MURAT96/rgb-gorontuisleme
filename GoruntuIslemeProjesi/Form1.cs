﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge;
using AForge.Imaging;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Imaging.Filters;



namespace GoruntuIslemeProjesi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private FilterInfoCollection CaptureDevices;
        private VideoCaptureDevice videoSource;

        private void Form1_Load(object sender, EventArgs e)
        {
            CaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo Device in CaptureDevices)
            {
                comboBox1.Items.Add(Device.Name);
            }
            comboBox1.SelectedIndex = 0;
            videoSource = new VideoCaptureDevice();
        }

        private void start_Click(object sender, EventArgs e)
        {
            videoSource = new VideoCaptureDevice(CaptureDevices[comboBox1.SelectedIndex].MonikerString);
            videoSource.NewFrame += new NewFrameEventHandler(VideoSource_NewFrame);
            videoSource.Start();
        }

        private void VideoSource_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            //IMAGE INDENTIFICATION 
            Bitmap image = (Bitmap)eventArgs.Frame.Clone();
            Bitmap image1 = (Bitmap)eventArgs.Frame.Clone();
            Bitmap image2 = (Bitmap)eventArgs.Frame.Clone();
            Bitmap image3 = (Bitmap)eventArgs.Frame.Clone();


            //Merror applaying 
            Mirror ayna = new Mirror(false, true);
            ayna.ApplyInPlace(image);
            ayna.ApplyInPlace(image1);
            ayna.ApplyInPlace(image2);
            ayna.ApplyInPlace(image3);


            //orginal video picturebox1 aktarmasi
            pictureBox1.Image = image;
            
            //euclidean filter
            EuclideanColorFiltering filter = new EuclideanColorFiltering();
            filter.CenterColor = new RGB(Color.FromArgb(0, 0, 170)); //blue 
            filter.Radius = 100;
            filter.ApplyInPlace(image1);
            filter.CenterColor = new RGB(Color.FromArgb(0, 140, 10)); //green
            filter.Radius = 100;
            filter.ApplyInPlace(image2);
            filter.CenterColor = new RGB(Color.FromArgb(170, 0, 0)); //red
            filter.Radius = 100;
            filter.ApplyInPlace(image3);

            //add filter
            Add topla = new Add(image2);
            Bitmap resultImage = topla.Apply(image1);
            Add topla1 = new Add(resultImage);
            Bitmap resultImage1 = topla1.Apply(image3);



            //picturebox2 transfer
            pictureBox2.Image = resultImage1;




        }
        
        
        

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
